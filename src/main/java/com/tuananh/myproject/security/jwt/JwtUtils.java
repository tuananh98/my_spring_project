package com.tuananh.myproject.security.jwt;

import com.tuananh.myproject.security.model.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    //Chuỗi bí mật để tạo ra jwt
    @Value("${configuration.app.jwtSecret}")
    private String jwtSecret;

    //Khoảng thời gian hết hạn của một chuỗi jwt
    @Value("${configuration.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    /**
     * Dùng để tạo ra một chuỗi jwt từ đối tượng Authentication
     *
     * @param authentication
     * @return chuỗi jwt
     */
    public String generateJwtToken(Authentication authentication) {
        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
        return generateTokenByUsername(userPrincipal.getUsername());
    }

    public String generateTokenByUsername(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Dùng để lấy username từ chuỗi jwt
     * @param token
     * @return username
     */
    public String getUserNameFromJwtToken(String token) {
        // Phương thức parseClaimsJws dùng để phân tích chuỗi token chuyền vào
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    /**
     * Dùng để kiểm tra chuỗi token có hợp lệ hay không
     * @param authToken
     * @return true/false
     */
    public boolean validateJwtToken(String authToken) {
        try {
            // Phương thức parseClaimsJws dùng để phân tích chuỗi token chuyền vào
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }
        return false;
    }

}
