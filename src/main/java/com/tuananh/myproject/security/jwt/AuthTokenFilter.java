package com.tuananh.myproject.security.jwt;

import com.tuananh.myproject.security.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Giải thích:
 * - OncePerRequestFilter: là đối tượng thực hiện một lần thực thi cho mỗi yêu cầu đối với API.
 * Nó cung cấp một doFilterInternal() phương thức mà chúng tôi sẽ triển khai phân tích cú pháp & xác thực JWT,
 * tải chi tiết Người dùng (sử dụng UserDetailsService), kiểm tra Authorizaion (sử dụng UsernamePasswordAuthenticationToken).
 *
 * - UsernamePasswordAuthenticationToken: là đối tượng lấy username, password từ login request và AuthenticationManager sẽ sử dụng nó để xác thực tài khoản login
 *
 * - SecurityContext:  là interface cốt lõi của Spring Security, lưu trữ tất cả các chi tiết liên quan đến bảo mật trong ứng dụng.
 * Khi chúng ta kích hoạt Spring Security trong ứng dụng thì SecurityContext cũng sẽ được kích hoạt theo
 *
 * - SecurityContextHolder: vì ta không thể truy cập trực tiếp vào SecurityContext nên Lớp này lưu trữ security context hiện tại của ứng dụng.
 */

/**
 * đối tượng AuthTokenFilter  dùng để định nghĩa một bộ lọc khi thực thi mỗi một request
 */

public class AuthTokenFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    /**
     * Dùng để triển khai phân tích cú pháp & xác thực JWT, tải chi tiết Người dùng (sử dụng UserDetailsService),
     * kiểm tra Authorizaion (sử dụng UsernamePasswordAuthenticationToken)
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            //1. Lấy jwt từ HttpServletRequest
            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                //2. Lấy username từ chuỗi jwt và lấy đối tượng UserDetails từ username đó
                String username = jwtUtils.getUserNameFromJwtToken(jwt);
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                //3. Tạo một đối tượng kiểm tra và lưu thông tin đăng nhập (UsernamePasswordAuthenticationToken) và set thông tin đó vào context của SecurityContextHolder
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            logger.error("Cannot set user authentication: {}", e);
        }
        filterChain.doFilter(request, response);
    }

    /**
     * Dùng để phân tích chuỗi jwt
     *
     * @param request
     * @return chuỗi jwt
     */
    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7, headerAuth.length());
        }
        return null;
    }
}
