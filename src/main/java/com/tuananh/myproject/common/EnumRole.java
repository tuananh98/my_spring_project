package com.tuananh.myproject.common;

public enum EnumRole {
    ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
