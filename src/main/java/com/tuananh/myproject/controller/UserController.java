package com.tuananh.myproject.controller;

import com.tuananh.myproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(name = "/")
    public ResponseEntity<?> getUsers(){
        return ResponseEntity.ok().body(userService.getAll());
    }
}
